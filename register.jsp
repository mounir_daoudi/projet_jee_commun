<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!-- saved from url=(0064)http://gdufrene.github.io/java_ee_spring-14/files/tp2/login.html -->
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="http://getbootstrap.com/favicon.ico">
      <title>Register</title>
      <!-- Bootstrap core CSS -->
      <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="http://gdufrene.github.io/java_ee_spring-14/files/tp2/signin.css" rel="stylesheet">
      <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
      <!--[if lt IE 9]><script src="http://getbootstrap.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
      <script src="./Signin Template for Bootstrap_files/ie-emulation-modes-warning.js"></script>
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
         <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
         <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
     	 <script src="./Signin Template for Bootstrap_files/ie10-viewport-bug-workaround.js"></script>
      <![endif]-->
   </head>

   <body>
      <div class="container">
         <center>
            <p>
		       <% if(request.getAttribute("error") != null) {
			   out.print("<span class =\"alert alert-danger\">" + request.getAttribute("error") +"</span>" );
		       }%>
		       <% if(request.getAttribute("success") != null) {
			   out.print("<span class =\"alert alert-success\">" + request.getAttribute("error") +"</span>" );
		       }%>
	        </p>
	     </center>
         <!--<form class="form-signin" role="form" method="post" action="signin">-->
	     <form:form action="register.html" method="post" commandName="userForm" class="form-signin" >
            <h2 class="form-signin-heading">Register now</h2>
            <label for="firstname" class="sr-only">First name</label>
            <input type="text" id="firstname" name ="firstname" placeholder="first name" autofocus=""
               value="<% if(request.getAttribute("firstname") != null) {
			      out.print(request.getAttribute("firstname"));
			   }%>"
		       class="form-control <% if(request.getAttribute("errorChampfirstname") != null) {
		          out.print(request.getAttribute("errorChampfirstname"));
			   }%>">
            <label for="lastname" class="sr-only">Last name</label>
            <input type="text" id="lastname" name ="lastname" placeholder="last name" autofocus=""
              value="<% if(request.getAttribute("lastname") != null) {
				 out.print(request.getAttribute("lastname"));
			  }%>"
		      class="form-control <% if(request.getAttribute("errorChamplastname") != null) {
				 out.print(request.getAttribute("errorChamplastname"));
			  }%>">
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" id="email" name ="email" placeholder="Email address" autofocus=""
              value="<% if(request.getAttribute("email") != null) {
				 out.print(request.getAttribute("email"));
			  }%>"
		      class="form-control <% if(request.getAttribute("errorChampemail") != null) {
				 out.print(request.getAttribute("errorChampemail"));
			  }%>">  
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="password" name="password" placeholder="Password" 
              class="form-control <% if(request.getAttribute("errorChamppassword") != null) {
				 out.print(request.getAttribute("errorChamppassword"));
			  }%>">
            <div class="checkbox">
              <label>
                 <input type="checkbox" value="remember-me"> Remember me
              </label>
            </div>
           <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </form:form>
        <center><label>if you already have an account <a href="./login.html">login here</a></label></center>
     </div>
   </body>
</html>