package dao;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import jpa.MemberDaoJPA;
import jpa.PasswordChangeRequestsDaoJPA;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import api.model.Member;
import api.model.PasswordChangeRequests;

public class DaoPasswordChangeRequestsTest {
	
	static EntityManager em;
	
	@BeforeClass
	public static void setupEntityManager() {
		em = new DaoTestConfig().getTestEntityManager();
	}
	
	MemberDaoJPA dao;
	PasswordChangeRequestsDaoJPA daoPwd;
	
	@Before
	public void initDao() {
		dao = new MemberDaoJPA();
		dao.em = em;
		
		daoPwd = new PasswordChangeRequestsDaoJPA() ;
		daoPwd.em = em;
		
		em.getTransaction().begin();
		em.createQuery("delete from PasswordChangeRequests").executeUpdate();
		em.getTransaction().commit();
	}
	
	@After
	public void closeTransaction() {
	}
	
	@Test
	public void testCreate() {
		Member member = dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", "Gf46fd654");
		Date d = new java.util.Date();
		PasswordChangeRequests pwd = daoPwd.create(d,"2H2F3DC4B45N5v", 0 ,member);
		assertEquals(d, pwd.time);
		assertEquals("2H2F3DC4B45N5v", pwd.uid);
		assertTrue("equal 0", 0 == pwd.alreadyUsed);
		assertEquals(member, pwd.member);

		assertTrue("pwd.id should be set", pwd.id > 0);
	}
	
	@Test
	public void testcheckUid() {
		String uid="Gf46fd6542635437";
		Date d = new java.util.Date();
		Member member = dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", uid);
		PasswordChangeRequests pwd = daoPwd.create(d,uid, 0 ,member);
		assertEquals(pwd, daoPwd.checkUid(uid));
	}
	
	public void testChangeAlreadyUsed() {
		String uid="Gf46fd6542635437";
		Date d = new java.util.Date();
		Member member = dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", uid);
		PasswordChangeRequests pwd = daoPwd.create(d,uid, 0 ,member);
		daoPwd.changeAlreadyUsed(uid);
		assertTrue("equal 1", 1 == daoPwd.checkUid(uid).alreadyUsed);

	}
	
}
	