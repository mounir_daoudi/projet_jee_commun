package dao;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;

import jpa.MemberDaoJPA;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import api.model.Member;

public class DaoMemberTest {
	
	static EntityManager em;
	
	@BeforeClass
	public static void setupEntityManager() {
		em = new DaoTestConfig().getTestEntityManager();
	}
	
	MemberDaoJPA dao;
	
	@Before
	public void initDao() {
		dao = new MemberDaoJPA();
		dao.em = em;
		//if(!em.getTransaction().isActive())
		em.getTransaction().begin();
		em.createQuery("delete from Member").executeUpdate();
		em.getTransaction().commit();
	}
	
	@After
	public void closeTransaction() {
		//if(em.getTransaction().isActive())
			//em.getTransaction().commit();
	}
	
	@Test
	public void testCreate() {
		Member member = dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", "gF54S8S");
		assertEquals("gdufrene@gmail.com", member.email);
		assertEquals("Guillaume", member.firstname);
		assertEquals("Dufrene", member.lastname);
		assertEquals(MemberDaoJPA.sha1("test"), member.password);
		assertTrue("Member.id should be set", member.id > 0);
	}
	
	@Test
	public void testListAll() {
		dao.create("bcathala@gmail.com","Bruno", "Cathala","test", "ssdd88");
		dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", "GSFS6S4D");
		dao.create("abauza@gmail.com","Antoine", "Bauza","test", "S6S5GDJD9S6");
		
		List<String> expectedNames = Arrays.asList( "Bauza", "Cathala", "Dufrene" );
		List<Member> members = dao.listAll();
		int len = expectedNames.size();
		assertEquals(len, members.size());
		for ( int i = 0; i < len; i++) {
			assertEquals("should order by lastname", expectedNames.get(i), members.get(i).lastname);
		}
	}
	
	@Test
	public void testFindById() throws Exception {
		assertNull( dao.findById(9999) );
		long id = dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", "sS5S6DHD").id;
		Member member = dao.findById(id);
		assertEquals("gdufrene@gmail.com", member.email);
		assertEquals("Guillaume", member.firstname);
		assertEquals("Dufrene", member.lastname);
		assertEquals(MemberDaoJPA.sha1("test"), member.password);
	}
	
	@Test
	public void testFindByEmail() throws Exception {
		//assertNull( dao.findByEmail("gdufrene@gmail.com") );
		String email = dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", "ssS6S5S").email;
		Member member = dao.findByEmail(email);
		assertEquals("gdufrene@gmail.com", member.email);
		assertEquals("Guillaume", member.firstname);
		assertEquals("Dufrene", member.lastname);
		assertEquals(MemberDaoJPA.sha1("test"), member.password);
	}
	
	@Test
	public void testUpdate() throws Exception {
		Member member = dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", "SFSD6S5S");
		member.firstname = "Maxime";
		dao.update(member);
		
		Member updated = dao.findById(member.id);
		assertEquals("gdufrene@gmail.com", updated.email);
		assertEquals("Maxime", updated.firstname);
		assertEquals("Dufrene", updated.lastname);
		assertEquals(MemberDaoJPA.sha1("test"), updated.password);
	}
	
	@Test
	public void testDelete() throws Exception {
		long id =dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", "SSTS6SS").id;
		Member existing = dao.findById(id);
		assertNotNull(existing);
		
		boolean ok = dao.delete(id);
		assertTrue(ok);
		
		Member deleted = dao.findById(id);
		assertNull(deleted);
	}
	
	@Test
	public void testDeleteAll() throws Exception {
		dao.create("bcathala@gmail.com","Bruno", "Cathala","test", "shhdd77S");
		dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", "S45S6D7D");
		dao.create("abauza@gmail.com","Antoine", "Bauza","test", "SGVSBS5S5");
		
		assertEquals( 3, dao.listAll().size() );
		dao.deleteAll();
		assertEquals( 0, dao.listAll().size() );
	}
	
	public void testConfirmAccount() throws Exception {
		String uid = "sGSGS65S28S7D7D3D23";
		long id = dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", uid).id;
		dao.confirmAccount(uid);
		Member activated = dao.findById(id);
		assertEquals((Integer)1, activated.isActive);
	}
	
	@Test
	public void testCheckMailPwd() throws Exception {
		String uid = "sGSGS65S28S7D7D3D";
		Member member = dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", uid);
		dao.confirmAccount(uid);
		boolean ok = dao.checkMailPwd(member.email, "test");
		assertTrue(ok);
	}
	
	@Test
	public void testChangePassword() throws Exception {
		String uid = "sGSGS65S28S7D7D3D23";
		long id = dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", uid).id;
		dao.confirmAccount(uid);
		dao.changePassword(id, "password");
		assertTrue(dao.checkMailPwd("gdufrene@gmail.com", "password"));
	}
	
	@Test
	public void testAddFriend() throws Exception {
		String uid = "sGSGS65S28S7D7D3D23";
		String uid2 = "sGSGS65S28S7D7D98S5";

		long id = dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", uid).id;
		long idFriend = dao.create("anejmeddine@gmail.com","Ayoub", "Nejmeddine","test", uid2).id;

		boolean ok = dao.addFriend(id, idFriend);
		assertTrue(ok);
	}
	
	@Test
	public void testRemoveFriend() throws Exception {
		String uid = "sGSGS65S28S7D7D3D23";
		String uid2 = "sGSGS65S28S7D7D98S5";

		long id = dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", uid).id;
		long idFriend = dao.create("anejmeddine@gmail.com","Ayoub", "Nejmeddine","test", uid2).id;

		boolean ok = dao.removeFriend(id, idFriend);
		assertTrue(ok);
	}
	
	
	@Test
	public void testListAllFriends() throws Exception {
		String uid = "sGSGS65S28S7D7D3D23";
		String uid2 = "sGSGS65S28S7D7D98S5";

		long id = dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", uid).id;
		long idFriend = dao.create("anejmeddine@gmail.com","Ayoub", "Nejmeddine","test", uid2).id;
		
		dao.addFriend(id, idFriend);
		Member friend = dao.findById(idFriend);

		List<Member> listFriend = dao.listAllFriends(id);
		assertEquals(listFriend.get(0), friend);
	}
	
	@Test
	public void testCheckAccountState() throws Exception {
		String uid = "sGSGS65S28S7D7D3D23";

		dao.create("gdufrene@gmail.com","Guillaume", "Dufrene","test", uid);
		boolean ok = dao.checkAccountState(uid);
		assertFalse(ok);
		dao.confirmAccount(uid);
		ok = dao.checkAccountState(uid);
		assertTrue(ok);
	}
	
	
	
}
