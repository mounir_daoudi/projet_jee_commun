package dao;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;

import jpa.AuthorDaoJPA;
import jpa.BoardGameDaoJPA;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import api.model.Author;
import api.model.BoardGame;

public class DaoBoardGameTest {
	
	static EntityManager em;
	
	@BeforeClass
	public static void setupEntityManager() {
		em = new DaoTestConfig().getTestEntityManager();
	}
	
	BoardGameDaoJPA dao;
	AuthorDaoJPA daoAuthor;
	
	@Before
	public void initDao() {
		dao = new BoardGameDaoJPA();
		dao.em = em;
		
		daoAuthor = new AuthorDaoJPA();
		daoAuthor.em = em; 
		
		em.getTransaction().begin();
		em.createQuery("delete from BoardGame").executeUpdate();
		em.getTransaction().commit();
	}
	
	@After
	public void closeTransaction() {
	}
	
	@Test
	public void testCreate() {
		BoardGame boardGame = dao.create("hero stick", 0, 0, 0, 0, 0, "description");
		assertEquals("hero stick", boardGame.name);
		assertEquals("description", boardGame.description);
		assertTrue("BoardGame.id should be set", boardGame.id > 0);
	}
	
	@Test
	public void testListAll() {
		dao.create("Fifa", 0, 0, 0, 0, 0, "PES");
		dao.create("PES", 0, 0, 0, 0, 0, "FIFA");
		dao.create("CallOFDuty", 0, 0, 0, 0, 0, "SuperMario");
		
		List<String> expectedNames = Arrays.asList( "CallOFDuty", "Fifa", "PES" );
		List<BoardGame> boardGame = dao.listAll();
		int len = expectedNames.size();
		assertEquals(len, boardGame.size());
		for ( int i = 0; i < len; i++) {
			assertEquals("should order by lastname", expectedNames.get(i), boardGame.get(i).name);
		}
	}
	
	@Test
	public void testFind() throws Exception {
		assertNull( dao.find(9999) );
		long id = dao.create("Fifa", 0, 0, 0, 0, 0, "Pes").id;
		BoardGame boardGame = dao.find(id);
		assertEquals("Fifa", boardGame.name);
		assertEquals("Pes", boardGame.description);
	}
	
	@Test
	public void testUpdate() throws Exception {
		BoardGame boardGame = dao.create("fifa", 0, 0, 0, 0, 0, "pes");
		boardGame.name = "mario";
		dao.update(boardGame);
		
		BoardGame updated = dao.find(boardGame.id);
		assertEquals("mario", updated.name);
		assertEquals("pes", updated.description);
	}
	
	@Test
	public void testDelete() throws Exception {
		long id = dao.create("FIFA", 0, 0, 0, 0, 0, "Socer").id;
		BoardGame existing = dao.find(id);
		assertNotNull(existing);
		
		boolean ok = dao.delete(id);
		assertTrue(ok);
		
		BoardGame deleted = dao.find(id);
		assertNull(deleted);
	}
	
	@Test
	public void testDeleteAll() throws Exception {
		dao.create("FIFA", 0, 0, 0, 0, 0, "PES");
		dao.create("FOOT", 0, 0, 0, 0, 0, "FOOT");
		dao.create("Tennis", 0, 0, 0, 0, 0, "Tennis");
		
		assertEquals( 3, dao.listAll().size() );
		dao.deleteAll();
		assertEquals( 0, dao.listAll().size() );
	}
	
	@Test
	public void testListAllByYear() throws Exception {
		dao.create("FIFA", 2012, 0, 0, 0, 0, "PES");
		assertEquals( "FIFA", dao.listAllGamesByYear(2012).get(0).name);
	}
	
	@Test
	public void testBoardGameAuthor() throws Exception {
		long id = dao.create("FIFA", 2012, 0, 0, 0, 0, "PES").id;
		assertEquals(0, dao.getAuthors(id).size());
	}
	/*		
	@Test
	public void testAddAuthor() throws Exception {
		long id = dao.create("FIFA", 2012, 0, 0, 0, 0, "PES").id;

		Author author  = daoAuthor.create("amine", "anas");
		//author.id = 10; 
		dao.addAuthor(id, author.id);
		
		assertEquals( 0, dao.getAuthors(id).size());
	}

public void addAuthor(Author author){
	authors.add(author);
}

public void removeAuthor(Author author){
	authors.remove(author);
}
*/
}
