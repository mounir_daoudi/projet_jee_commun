package dao;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;

import jpa.AuthorDaoJPA;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import api.model.Author;

public class DaoAuthorTest {
	
	static EntityManager em;
	
	@BeforeClass
	public static void setupEntityManager() {
		em = new DaoTestConfig().getTestEntityManager();
	}
	
	AuthorDaoJPA dao;
	
	@Before
	public void initDao() {
		dao = new AuthorDaoJPA();
		dao.em = em;
		
		em.getTransaction().begin();
		em.createQuery("delete from Author").executeUpdate();
		em.getTransaction().commit();
	}
	
	@After
	public void closeTransaction() {
	}
	
	@Test
	public void testCreate() {
		Author author = dao.create("Guillaume", "Dufrene");
		assertEquals("Guillaume", author.firstname);
		assertEquals("Dufrene", author.lastname);
		assertTrue("Author.id should be set", author.id > 0);
	}
	
	@Test
	public void testListAll() {
		dao.create("Bruno", "Cathala");
		dao.create("Guillaume", "Dufrene");
		dao.create("Antoine", "Bauza");
		
		List<String> expectedNames = Arrays.asList( "Bauza", "Cathala", "Dufrene" );
		List<Author> authors = dao.listAll();
		int len = expectedNames.size();
		assertEquals(len, authors.size());
		for ( int i = 0; i < len; i++) {
			assertEquals("should order by lastname", expectedNames.get(i), authors.get(i).lastname);
		}
	}
	
	@Test
	public void testFind() throws Exception {
		assertNull( dao.find(9999) );
		long id = dao.create("Guillaume", "Dufrene").id;
		Author author = dao.find(id);
		assertEquals("Guillaume", author.firstname);
		assertEquals("Dufrene", author.lastname);
	}
	
	@Test
	public void testUpdate() throws Exception {
		Author author = dao.create("Guillaume", "Dufrene");
		author.firstname = "Maxime";
		dao.update(author);
		
		Author updated = dao.find(author.id);
		assertEquals("Maxime", updated.firstname);
		assertEquals("Dufrene", updated.lastname);
	}
	
	@Test
	public void testDelete() throws Exception {
		long id = dao.create("Guillaume", "Dufrene").id;
		Author existing = dao.find(id);
		assertNotNull(existing);
		
		boolean ok = dao.delete(id);
		assertTrue(ok);
		
		Author deleted = dao.find(id);
		assertNull(deleted);
	}
	
	@Test
	public void testDeleteAll() throws Exception {
		dao.create("Bruno", "Cathala");
		dao.create("Guillaume", "Dufrene");
		dao.create("Antoine", "Bauza");
		
		assertEquals( 3, dao.listAll().size() );
		dao.deleteAll();
		assertEquals( 0, dao.listAll().size() );
	}

}
