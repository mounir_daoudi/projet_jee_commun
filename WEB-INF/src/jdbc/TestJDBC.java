package jdbc;
import java.sql.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class TestJDBC implements ITestJDBC {
  
	Connection conn;
  
	public Connection getConnection() {
		return conn;
	}
  
	// JDBC1 ******************************
	public void connect() throws SQLException, ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		conn = DriverManager.getConnection("jdbc:sqlite:data.db");
	}
  
	public boolean checkPasswordBasic( String email, String pwd ) 
		  throws SQLException {
		PreparedStatement preparedStatement = conn.prepareStatement(
				"SELECT count(*) FROM member where email=? and pwd=? ;");
		preparedStatement.setQueryTimeout(30);
		preparedStatement.setString(1, email);
		preparedStatement.setString(2, pwd);
    
		// Request to search for member
	    ResultSet rs = preparedStatement.executeQuery();
	    if(rs.getInt(1) != 0) {
		    return true;
		} else {
			return false;
		}
	}
  
  // JDBC2 ******************************
  public boolean exists(String email) throws SQLException {
	  PreparedStatement preparedStatement = conn.prepareStatement(
			  "SELECT count(*) FROM member where email= ?;");
	  preparedStatement.setQueryTimeout(30);
	  preparedStatement.setString(1, email);
	
	  // Request to see if a member exists (using email address)
	  ResultSet rs = preparedStatement.executeQuery();

	  if(rs.getInt(1) != 0) {
		  rs.close();
		  return true;
	  } else {
		  rs.close();
		  return false;
	  }
  }
  
  public void addMember(String lastname, String firstname, String email, String password) throws SQLException {
	  if(exists(email)) {
		  throw new SQLException();
	  }
	  
	  PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO member VALUES(?,?,?,?);");
	  
	  preparedStatement.setQueryTimeout(30);
	  preparedStatement.setString(1, lastname);
	  preparedStatement.setString(2, firstname);
	  preparedStatement.setString(3, email);
	  preparedStatement.setString(4, sha1(password));
	  

	  // Request to insert data in member table
	  int statut = preparedStatement.executeUpdate();
		  
	  System.out.println( " ==> R�sultat de la requ�te d'insertion : " + statut );
  }
  
  public boolean checkPasswordWithSum( String email, String pwd ) 
		  throws SQLException {  
	  return checkPasswordBasic(email,sha1(pwd));
  }
  
  public static void main(String argv[]) throws Exception {
    TestJDBC db = new TestJDBC();
    db.connect();
    
    if(!db.exists("guillaume.dufrene@webpulser.com")) {
    	db.addMember("DUFRENE", "Guillaume", "guillaume.dufrene@webpulser.com", "test");
    }
    
    System.out.println( "Test d'identifiant");
    System.out.println( " ==> Exists : " + db.exists("guillaume.dufrene@webpulser.com"));
    System.out.println( " ==> Exists : " + db.exists("mdaoudi@hotmail.com"));
    System.out.println( " ==> Exists : " + db.exists("mdaoudi2@hotmail.com"));
    
    System.out.println( "Test du mot de passe");
    System.out.println( " ==> Matches : " + db.checkPasswordBasic( 
    		"gdufrene@gmail.com", "test" ));
    System.out.println( " ==> Matches : " + db.checkPasswordWithSum( 
    	"mdaoudi@hotmail.com", "doudou59" ));
    System.out.println( " ==> Matches : " + db.checkPasswordWithSum( 
        	"mdaoudi2@hotmail.com", "doudou" ));
  }
  
  private static String sha1(String password) {
	  MessageDigest md = null;
	  try {
	      md = MessageDigest.getInstance("SHA-1");
	  } catch(NoSuchAlgorithmException e) {
	      e.printStackTrace();
	      return "*****";
	  } 
	  byte[] sha1sum = md.digest(password.getBytes());
	  StringBuffer sb = new StringBuffer();
	  for( int i = 0; i < sha1sum.length; i++ ) sb.append( String.format("%02x", sha1sum[i]) );
	  return sb.toString();
	}
}