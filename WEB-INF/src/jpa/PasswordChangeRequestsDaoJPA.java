package jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import api.dao.PasswordChangeRequestsDao;
import api.model.Member;
import api.model.PasswordChangeRequests;

@Component
public class PasswordChangeRequestsDaoJPA implements PasswordChangeRequestsDao {
	@Autowired
	public EntityManager em;
	
	@Override
	public PasswordChangeRequests create(Date time, String uid,
			Integer alreadyUsed, Member member) {
		PasswordChangeRequests pwd = PasswordChangeRequests.create(time, uid, alreadyUsed, member );
		em.getTransaction().begin();
		em.persist(pwd);
		em.getTransaction().commit();
		return pwd;
	}
	
	@Override
	public PasswordChangeRequests checkUid(String uid) {
		List<PasswordChangeRequests> results = (List<PasswordChangeRequests>) em.createQuery("SELECT p FROM PasswordChangeRequests p WHERE p.uid = :uid and p.alreadyUsed = 0").setParameter("uid", uid).getResultList();
		if (!results.isEmpty()){
			return results.get(0);
		}
		return null;
	}
	
	@Override
	public int changeAlreadyUsed(String uid){
		try {
			em.getTransaction().begin();
			Query query = em.createQuery("UPDATE PasswordChangeRequests SET alreadyUsed = 1 where uid = :uid").setParameter("uid", uid);
			int updateCount = query.executeUpdate();
			em.getTransaction().commit();
			return updateCount;
		} catch (Exception e) {
			em.getTransaction().commit();
			return 0;
		}
	}

}
