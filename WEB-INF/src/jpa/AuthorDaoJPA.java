package jpa;

import java.util.List;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import api.dao.AuthorDao;
import api.model.Author;

@Component
public class AuthorDaoJPA implements AuthorDao {
	@Autowired
	public EntityManager em;
	
	/* (non-Javadoc)
	 * @see jpa.AuthorDao#create(java.lang.String, java.lang.String)
	 */
	@Override
	public Author create(String firstName, String lastName) {
		em.getTransaction().begin();
		Author author = Author.create(firstName, lastName);
		em.persist(author);
		em.getTransaction().commit();
		return author;
	}
	
	/* (non-Javadoc)
	 * @see jpa.AuthorDao#listAll()
	 */
	@Override
	public List<Author> listAll() {
		Query query = em.createQuery("SELECT e FROM Author e ORDER BY lastname");

		return (List<Author>)query.getResultList();
	}
	
	/* (non-Javadoc)
	 * @see jpa.AuthorDao#find(long)
	 */
	@Override
	public Author find(long id) {
		Author result = em.find(Author.class, id);

		return result;
	}
	
	/* (non-Javadoc)
	 * @see jpa.AuthorDao#update(api.model.Author)
	 */
	@Override
	public boolean update( Author author ) {
		try {
			em.getTransaction().begin();
			em.merge(author);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			em.getTransaction().commit();
			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see jpa.AuthorDao#delete(long)
	 */
	@Override
	public boolean delete( long id ) {
		try {
			Author author = em.find(Author.class, id);
			em.getTransaction().begin();
			em.remove(author);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see jpa.AuthorDao#deleteAll()
	 */
	@Override
	public boolean deleteAll( ) {
		try {
			em.getTransaction().begin();
			Query query = em.createQuery("DELETE FROM Author");
			query.executeUpdate();
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}