package jpa;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import api.dao.MemberDao;
import api.model.Member;

@Component
public class MemberDaoJPA implements MemberDao {
	@Autowired
	public EntityManager em;

	@Override
	public Member create(String email, String firstName, String lastName, String password, String uid) {
		em.getTransaction().begin();
		Member member = Member.create(email, firstName, lastName, MemberDaoJPA.sha1(password), 0, uid);
		em.persist(member);
		em.getTransaction().commit();
		return member;
	}

	@Override
	public List<Member> listAll() {
		Query query = em.createQuery("SELECT e FROM Member e ORDER BY lastname");
		return (List<Member>)query.getResultList();
	}

	@Override
	public List<Member> listWithOutFriend(List<Member> friend,Member current) {
		int i;
		String sql = "SELECT e FROM Member e WHERE e.id NOT IN(";
		sql += current.getId();
		for (i=0;i<friend.size();i++)
		{
			sql += ","+friend.get(i).getId();
		}
		sql += ")";
		sql += " ORDER BY lastname";
		Query query = em.createQuery(sql);
		return (List<Member>)query.getResultList();
	}

	@Override
	public Member findById(long id) {
		Member result = em.find(Member.class, id);
		return result;
	}

	@Override
	public Member findByEmail(String email) {
		List<Member> results = (List<Member>) em.createQuery("SELECT e FROM Member e WHERE e.email = :email").setParameter("email", email).getResultList();
		if (!results.isEmpty()){
			return results.get(0);
		}
		return null;
	}

	@Override
	public boolean update(Member member) {
		try {
			em.getTransaction().begin();
			em.merge(member);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
		//	em.getTransaction().commit();
			return false;
		}
	}

	@Override
	public boolean delete( long id ) {
		try {
			Member member = em.find(Member.class, id);
			em.getTransaction().begin();
			em.remove(member);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			em.getTransaction().commit();
			return false;
		}
	}

	@Override
	public boolean deleteAll( ) {
		try {
			em.getTransaction().begin();
			Query query = em.createQuery("DELETE FROM Member");
			query.executeUpdate();
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			em.getTransaction().commit();
			return false;
		}
	}

	@Override
	public boolean checkMailPwd(String email, String password) {
		List<Member> results = (List<Member>) em.createQuery("SELECT e FROM Member e WHERE e.email = :email and e.password = :password and e.isActive = :isActive")
				.setParameter("email", email)
				.setParameter("password", MemberDaoJPA.sha1(password))
				.setParameter("isActive", 1)
				.getResultList();
		
		return (!results.isEmpty());

	}

	@Override
	public boolean removeFriend(long id,long idFriend) {
		Member friend = findById(idFriend);
		if (friend != null) {
			Member member = findById(id);
			member.removeFriend(friend);
			update(member);
			return true;
		} else {
			return false;
		}		
	}

	@Override
	public boolean addFriend(long id,long idFriend) {
		Member friend = findById(idFriend);
		if (friend != null) {
			Member member = findById(id);
			member.addFriend(friend);
			this.update(member);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<Member> listAllFriends(long id) {
		Member member = findById(id);
		if(member == null) {
			return null;
		}
		return member.getAmis();
	}

	@Override
	public int confirmAccount(String uid){
		int updateCount = 0;
		//Vérifier si le compte est déja activée
		if(this.checkAccountState(uid)){
			return updateCount;
		}
		try {
			em.getTransaction().begin();
			Query query = em.createQuery("UPDATE Member SET isActive = 1 where uid = :uid").setParameter("uid", uid);
			updateCount = query.executeUpdate();
			em.getTransaction().commit();
			return updateCount;
		} catch (Exception e) {
			em.getTransaction().commit();
			return 0;
		}
	}

	@Override
	public int changePassword(long id, String password){
		try {
			em.getTransaction().begin();
			Query query = em.createQuery("UPDATE Member SET password = :password where id = :id").setParameter("password", MemberDaoJPA.sha1(password))
					.setParameter("id", id);
			int updateCount = query.executeUpdate();
			em.getTransaction().commit();
			return updateCount;
		} catch (Exception e) {
			return 0;
		}
	}


	@Override
	public boolean checkAccountState(String uid) {
		List<Member> results = (List<Member>) em.createQuery("SELECT e FROM Member e WHERE e.uid = :uid and e.isActive = :isActive")
				.setParameter("uid", uid)
				.setParameter("isActive", 1)
				.getResultList();
		if(!results.isEmpty()){
			return true;
		}
		return false;
	}

	/**
	 * Sha1 : crypte de mot de passe
	 * @param password
	 * @return
	 */
	public static String sha1(String password) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-1");
		} catch(NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "*****";
		} 
		byte[] sha1sum = md.digest(password.getBytes());
		StringBuffer sb = new StringBuffer();
		for( int i = 0; i < sha1sum.length; i++ ) sb.append( String.format("%02x", sha1sum[i]) );
		return sb.toString();
	}

}