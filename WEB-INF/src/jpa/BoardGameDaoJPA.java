package jpa;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import api.dao.AuthorDao;
import api.dao.BoardGameDao;
import api.model.Author;
import api.model.BoardGame;
import api.model.Member;

@Component
@ComponentScan({"impl","jpa"})
public class BoardGameDaoJPA implements BoardGameDao {
	@Autowired
	public EntityManager em;
	@Autowired
	AuthorDao authorDao;
	
	@Override
	public BoardGame create(String name, int publicationYear, int minPlayer, int maxPlayer, int playingTime, int suggestedAge, String description) {
		em.getTransaction().begin();
		BoardGame boardGame = BoardGame.create(name, publicationYear, minPlayer, maxPlayer, playingTime, suggestedAge, description);
		em.persist(boardGame);
		em.getTransaction().commit();
		return boardGame;
	}

	@Override
	public List<BoardGame> listAllGamesByYear(int year) {
		List<BoardGame> boardGames =  em.createQuery("SELECT e FROM BoardGame e WHERE e.publicationYear = :year").setParameter("year", year).getResultList();
		return boardGames;
	}

	@Override
	public boolean update(BoardGame game) {
		try {
			em.getTransaction().begin();
			em.merge(game);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public List<BoardGame> listAll() {
		List<BoardGame> boardGames = em.createQuery("SELECT e FROM BoardGame e ORDER BY name").getResultList();
		return boardGames;
	}

	@Override
	public BoardGame find(long id) {
		BoardGame result = em.find(BoardGame.class, id);
		return result;
	}

	@Override
	public boolean delete(long id) {
		try {
			BoardGame boardGame = em.find(BoardGame.class, id);
			em.getTransaction().begin();
			em.remove(boardGame);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean deleteAll() {
		try {
			em.getTransaction().begin();
			Query query = em.createQuery("DELETE FROM BoardGame");
			query.executeUpdate();
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	@Override
	public boolean removeAuthor(long id,long idAuthor) {
		Author author = authorDao.find(idAuthor);
		BoardGame boardgame = find(id);
		boardgame.removeAuthor(author);
		update(boardgame);
		return true;	
	}

	@Override
	public boolean addAuthor(long id,long idAuthor) {
		Author author = authorDao.find(idAuthor);
		if (author != null) {
			BoardGame boardGame = find(id);
			boardGame.addAuthor(author);
			update(boardGame);
			return true;
		}
		else {
			return false;
		}
	}
	
	@Override
	public List<Author> getAuthors(long id) {
		BoardGame boardGame = find(id);
		return boardGame.getAuthors();
	}

}