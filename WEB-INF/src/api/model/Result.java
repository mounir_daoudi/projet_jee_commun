package api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="results")
@IdClass(Result.class)
public class Result implements Serializable{
	
	@Id
	@Column(name="matches_id")
	public long idMatch;
	
	@Id
	@Column(name="members_id")
	public long idMember;
	
	@Column(name="results_score" /*, nullable=false*/)
	public int score;
	
	@Column(name="results_rank" /*, nullable=false*/)
	public int rank;
	
	@Column(name="results_pointsBalance" /*, nullable=false*/)
	public int pointsBalance;
}
