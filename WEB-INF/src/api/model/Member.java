package api.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="members")
public class Member {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public long id;
	
	@Column(name="email" /*, nullable=false*/)
	public String email;
	
	@Column(name="firstname" /*, nullable=false*/)
	public String firstname;
	
	@Column(name="lastname" /*, nullable=false*/)
	public String lastname;
	
	@Column(name="password" /*, nullable=false*/)
	public String password;
	
	@Column(nullable=true, name="isActive")
	public Integer isActive;
	
	@Column(name="uid" /*, nullable=false*/)
	public String uid;

	@OneToMany(fetch=FetchType.LAZY)
	public List<MemberGame> games = new ArrayList<>();
	
	@OneToMany(fetch=FetchType.LAZY)
	public List<SiteAccount> accounts = new ArrayList<>();
	
	@OneToMany(fetch=FetchType.LAZY)
	public List<Match> matchs = new ArrayList<>();

	@OneToMany(fetch=FetchType.LAZY)
	public List<Member> amis = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Integer getIsActive(){
		return this.isActive;
	}
	
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	
	public String getUid() {
		return this.uid;
	}
	
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public List<Member> getAmis() {
		if(this.amis == null)
		{
			this.amis = new ArrayList<Member>();
		}
		return this.amis;
	}
	
	public void setAmis(List<Member> amis) {
		this.amis = amis;
	}
	
	public static Member create(String email, String firstName, 
			String lastName, String password, int isActive, String uid) {
		Member member = new Member();
		member.email = email;
		member.firstname = firstName;
		member.lastname = lastName;
		member.password = password;
		member.isActive = isActive;
		member.uid = uid;
		return member;
	}
	
	public void addFriend(Member friend){
		amis.add(friend);
	}
	
	public void removeFriend(Member friend){
		amis.remove(friend);
	}
}
