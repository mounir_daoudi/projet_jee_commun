package api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="memberGames")
public class MemberGame {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public long id;
	
	@Column(name="own" /*, nullable=false*/)
	public boolean own;
	
	@Column(name="note" /*, nullable=false*/)
	public int note;
	
	@Column(name="partiesPlayed" /*, nullable=false*/)
	public int partiesPlayed;
	
	@Column(name="points" /*, nullable=false*/)
	public int points;
	
	@ManyToOne
	public BoardGame boardGame;
	
	public Enum<Rank> rank;
	
}
