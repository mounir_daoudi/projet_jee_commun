package api.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="boardGames")
public class BoardGame {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id") 
	public long id;
	
	@Column(name="name" /*, nullable=false*/)
	public String name;
	
	@Column(name="publicationYear" /*, nullable=false*/)
	public int publicationYear;
	
	@Column(name="minPlayer" /*, nullable=false*/)
	public int minPlayer;
	
	@Column(name="maxPlayer" /*, nullable=false*/)
	public int maxPlayer;
	
	@Column(name="playingTime" /*, nullable=false*/)
	public int playingTime;
	
	@Column(name="suggestedAge" /*, nullable=false*/)
	public int suggestedAge;
	
	@Column(name="description", columnDefinition="text" /*, nullable=false*/)
	public String description;
	
	@ManyToMany(fetch=FetchType.LAZY)
	public List<Author> authors = new ArrayList<>();
	
	@OneToMany(fetch=FetchType.LAZY)
	public List<Extension> extentions = new ArrayList<>();
	
	public List<Author> getAuthors() {
		if(this.authors == null)
		{
			this.authors = new ArrayList<Author>();
		}
		return this.authors;
	}
	
	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}
	
	public static BoardGame create(String name, int publicationYear, int minPlayer, int maxPlayer, int playingTime, int suggestedAge, String description) {
		BoardGame boardGame = new BoardGame();
		boardGame.name = name;
		boardGame.publicationYear = publicationYear;
		boardGame.minPlayer = minPlayer;
		boardGame.maxPlayer = maxPlayer;
		boardGame.playingTime = playingTime;
		boardGame.suggestedAge = suggestedAge;
		boardGame.description = description;
		return boardGame;
	}
	
	@Override
	public String toString() {
		return "BoardGame #" + this.id +  " [" + name + "]\n" 
				+ ( publicationYear > 0 ? "   published: " + publicationYear + "\n" : "" ) 
				+ ( suggestedAge > 0    ? "   suggested age from: " + suggestedAge + "\n" : "" )
				+ ( minPlayer > 0 && maxPlayer > 0 ? "   nb of players: " + minPlayer + " to " + maxPlayer + "\n" : "" )
				+ ( playingTime > 0     ? "   playing time: " + playingTime + "\n" : "")
				+ ( authors.size() > 0  ? "   authors: " + authors : "" )
				;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getPublicationYear() {
		return publicationYear;
	}

	public void setPublicationYear(int publicationYear) {
		this.publicationYear = publicationYear;
	}

	public int getMinPlayer() {
		return minPlayer;
	}

	public void setMinPlayer(int minPlayer) {
		this.minPlayer = minPlayer;
	}

	public int getMaxPlayer() {
		return maxPlayer;
	}

	public void setMaxPlayer(int maxPlayer) {
		this.maxPlayer = maxPlayer;
	}

	public int getPlayingTime() {
		return playingTime;
	}

	public void setPlayingTime(int playingTime) {
		this.playingTime = playingTime;
	}

	public int getSuggestedAge() {
		return suggestedAge;
	}

	public void setSuggestedAge(int suggestedAge) {
		this.suggestedAge = suggestedAge;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Extension> getExtentions() {
		return extentions;
	}

	public void setExtentions(List<Extension> extentions) {
		this.extentions = extentions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void addAuthor(Author author){
		authors.add(author);
	}
	
	public void removeAuthor(Author author){
		authors.remove(author);
	}
}