package api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="PasswordChangeRequests")
public class PasswordChangeRequests {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id") 
	public long id;

	@Column(name="time" /*, nullable=false*/)
	public Date time;

	@Column(name="uid" /*, nullable=false*/)
	public String uid;

	@Column(name="alreadyUsed" /*, nullable=false*/)
	public Integer alreadyUsed;
	
	@ManyToOne
	public Member member;
	
	public long getId(){
		return this.id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	public Date getTime(){
		return this.time;
	}
	
	public void setTime(Date time){
		this.time = time;
	}

	public String getUid(){
		return this.uid;
	}
	
	public void setId(String uid){
		this.uid = uid;
	}
	
	public Integer getAlreadyUsed(){
		return this.alreadyUsed;
	}
	
	public void setAlreadyUsed(Integer alreadyUsed){
		this.alreadyUsed = alreadyUsed;
	}
	
	public Member getMember(){
		return this.member;
	}
	
	public void setMember(Member member){
		this.member = member;
	}
	/**
	 * create
	 * @param time
	 * @param uid
	 * @param alreadyUsed
	 * @param member
	 * @return
	 */
	public static PasswordChangeRequests create(Date time, String uid,
			Integer alreadyUsed, Member member) {
		
		PasswordChangeRequests pwd = new PasswordChangeRequests();
		pwd.time = time;
		pwd.uid = uid;
		pwd.alreadyUsed = alreadyUsed;
		pwd.member = member;
		
		return pwd;
	}

}
