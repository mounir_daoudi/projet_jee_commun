package api.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="phases")
public class Phase {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public long id;
	
	@Column(name="name" /*, nullable=false*/)
	public String name;
	
	@OneToMany(fetch=FetchType.LAZY)
	public List<Match> matchs = new ArrayList<>();
}
