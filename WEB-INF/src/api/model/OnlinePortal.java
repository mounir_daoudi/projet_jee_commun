package api.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="onlinePortals")
public class OnlinePortal {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public long id;
	
	@Column(name="name" /*, nullable=false*/)
	public String name;
	
	@Column(name="url" /*, nullable=false*/)
	public String url;
	
	@ManyToMany
	public List<BoardGame> games = new ArrayList<>();
}
