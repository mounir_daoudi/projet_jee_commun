package api.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="authors")
public class Author {
	
	/*@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="JOIN_AUTHOR_BOARDGAME",
			joinColumns ={@JoinColumn(name="authorId")},
			inverseJoinColumns={@JoinColumn(name="boardGameId")})
	public List<BoardGame> boardGames = new ArrayList<>();
	*/
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id") 
	public long id;
	
	@Column(name="firstname" /*, nullable=false*/)
	public String firstname;
	
	@Column(name="lastname" /*, nullable=false*/)
	public String lastname;
	
	@Override
	public String toString() {
		return firstname + " " + lastname;
	}
	
	public static Author create(String firstName, String lastName) {
		Author author = new Author();
		author.firstname = firstName;
		author.lastname = lastName;
		return author;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id	;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
}