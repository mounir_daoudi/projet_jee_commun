package api.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tournaments")
public class Tournament {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="tournaments_id")
	public long id;
	
	@Column(name="name" /*, nullable=false*/)
	public String name;
	
	@Column(name="fromDate" /*, nullable=false*/)
	public Date fromDate;
	
	@Column(name="toDate" /*, nullable=false*/)
	public Date toDate;
	
	@Column(name="minLevel" /*, nullable=false*/)
	public int minLevel;
	
	@Column(name="maxLevel" /*, nullable=false*/)
	public int maxLevel;

	@ManyToOne
	public OnlinePortal portal;
	
	@OneToMany
	public List<Phase> phases = new ArrayList<>();
	
}
