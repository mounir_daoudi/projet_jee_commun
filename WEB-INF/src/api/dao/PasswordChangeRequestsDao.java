package api.dao;

import java.util.Date;

import api.model.Member;
import api.model.PasswordChangeRequests;

public interface PasswordChangeRequestsDao {
	
	public abstract PasswordChangeRequests create(Date time, String uid, Integer alreadyUsed, Member member);

	public PasswordChangeRequests checkUid(String uid);

	public int changeAlreadyUsed(String uid);

}