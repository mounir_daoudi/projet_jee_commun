package api.dao;

import java.util.List;

import api.model.Member;

public interface MemberDao {

	/**
	 * Create a new Member
	 * @param email
	 * @param firstname
	 * @param lastName
	 * @param password
	 * @param uid
	 * @return created Member with ID
	 */
	public abstract Member create(String email, String firstName,
			String lastName, String password, String uid);

	/**
	 * get all members ordered by lastname, firstname
	 * @return members
	 */
	public abstract List<Member> listAll();

	/**
	 * Get a member by its ID
	 * @param id
	 * @return member
	 */
	public abstract Member findById(long id);
	
	/**
	 * Get a member by its EMAIL
	 * @param email
	 * @return member
	 */
	public abstract Member findByEmail(String email);

	/**
	 * Update member firstname and/or lastname
	 * @param member
	 * @return true if succeed
	 */
	public abstract boolean update(Member member);

	/**
	 * Delete a member by its ID
	 * @param id
	 * @return true if succeed
	 */
	public abstract boolean delete(long id);

	/**
	 * delete all members
	 * @return true if succeed
	 */
	public abstract boolean deleteAll();
	/**
	* remove a friend by its ID
	* @param id
	* @return true if succeed
	*/
	public abstract boolean removeFriend(long id,long idFriend);

	/**
	* add a friend by its ID
	* @param id
	* @return true if succeed
	*/
	public abstract boolean addFriend(long id,long idFriend);

	/**
	* list of friends 
	* @param id
	* @return true if succeed
	*/
	public abstract List<Member> listAllFriends(long id);
	
	/**
	 * check mail and password for connection
	 * @return true if succeed
	 */
	public abstract boolean checkMailPwd(String email, String password);

	/**
	 * confirmAccount
	 * @param uid
	 * @return
	 */
	int confirmAccount(String uid);
	
	/**
	 * changePassword
	 * @param id
	 * @param password
	 * @return
	 */
	int changePassword(long id, String password);
	
	/**
	* list of member 
	* @param listOfFriend
	* @return  list of member
	*/
	public List<Member> listWithOutFriend(List<Member> friend, Member current);
	
	/**
	 * checkAccountState
	 * @param uid
	 * @return
	 */
	boolean checkAccountState(String uid);

}