package api.dao;

import java.util.List;

import api.model.Author;
import api.model.BoardGame;

public interface BoardGameDao {

	/**
	 * Create a new BoardGame
	 * @param name
	 * @param publicationYear
	 * @param minPlayer
	 * @param maxPlayer
	 * @param playingTime
	 * @param suggestedAge
	 * @param description
	 * @return created Author with ID
	 */
	public abstract BoardGame create(String name, int publicationYear, int minPlayer, int maxPlayer, int playingTime, int suggestedAge, String description);

	/**
	 * get all BoardGAmes ordered by lastname, firstname
	 * @return BoardGAmes
	 */
	public abstract List<BoardGame> listAll();
	
	/**
	 * get all BoardGAmes by year
	 * @param year
	 * @return BoardGAmes
	 */
	public abstract List<BoardGame> listAllGamesByYear(int year);

	/**
	 * Get an BoardGAmes by the author ID
	 * @param id
	 * @return BoardGame
	 */
	public abstract BoardGame find(long id);

	/**
	 * Update BoardGAmes
	 * @param BoardGame
	 * @return true if succeed
	 */
	public abstract boolean update(BoardGame game);

	/**
	 * Delete an BoardGame by its ID
	 * @param id
	 * @return true if succeed
	 */
	public abstract boolean delete(long id);

	/**
	 * delete all BoardGames
	 * @return true if succeed
	 */
	public abstract boolean deleteAll();

	boolean removeAuthor(long id, long idAuthor);

	boolean addAuthor(long id, long idAuthor);
	
	List<Author> getAuthors(long id);

}