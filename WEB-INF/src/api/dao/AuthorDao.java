package api.dao;

import java.util.List;

import api.model.Author;

public interface AuthorDao {

	/**
	 * Create a new Author
	 * @param firstName
	 * @param lastName
	 * @return created Author with ID
	 */
	public abstract Author create(String firstName, String lastName);

	/**
	 * get all authors ordered by lastname, firstname
	 * @return authors
	 */
	public abstract List<Author> listAll();

	/**
	 * Get an author by its ID
	 * @param id
	 * @return author
	 */
	public abstract Author find(long id);

	/**
	 * Update author firstname and/or lastname
	 * @param author
	 * @return true if succeed
	 */
	public abstract boolean update(Author author);

	/**
	 * Delete an author by its ID
	 * @param id
	 * @return true if succeed
	 */
	public abstract boolean delete(long id);

	/**
	 * delete all authors
	 * @return true if succeed
	 */
	public abstract boolean deleteAll();

}