package fr.eservice.servlet;

  import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/circ")
public class Circonference extends HttpServlet {
  	private static final long serialVersionUID = 1L;

    @Override
  	protected void service( HttpServletRequest req, HttpServletResponse resp ) 
  		throws ServletException, IOException {
  		resp.setCharacterEncoding("UTF-8");

  		String rayon = req.getParameter("rayon");
  		
  		if ( rayon == null || rayon.isEmpty() ) {
  			resp.setContentType("text/plain");
  			resp.getWriter().write( 
  				req.getRequestURI() + " Utilisation:\n" +
  				"\thttp://" + req.getServerName() + ":" + req.getServerPort() + req.getRequestURI() + "?rayon=X\n" +
  				"\tretourne la circonf�rence d'un cercle dont le rayon est donn�.\n" +
  				"\trayon: un nombre flottant\n"
  			);
  			return;
  		}

  		double d;
  		try {
  			d = Double.parseDouble(rayon);
  		} catch( NumberFormatException exp ) {
  			resp.setContentType("text/plain");
  			resp.getWriter().write( "Le rayon indiqu� ["+rayon+"] ne peut pas �tre lu.\n" );
  			return;
  		}

  		resp.setContentType("application/json");
  		
  		double circ = Math.PI * 2 *d;
  		
  		resp.getWriter().write( "circ=" + circ );
  	}
  }