package fr.eservice.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import jdbc.TestJDBC;

@WebServlet("/signin")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestJDBC jdbc;

	public void init() throws ServletException {
		jdbc = new TestJDBC();

		try {
			jdbc.connect();
		} catch (ClassNotFoundException e) {
			System.out.print(e.getMessage());
		} catch (SQLException e) {
			System.out.print(e.getMessage());
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Set response content type
		response.setContentType("text/html");
		
		try {
			if (jdbc.exists(request.getParameter("email"))) {
				request.setAttribute("error", "This mail is already registred");
				request.setAttribute("lastname", request.getParameter("lastname"));
				request.setAttribute("firstname", request.getParameter("firstname"));
				request.setAttribute("email", request.getParameter("email"));
				
				request.getRequestDispatcher("/register.jsp").forward(request, response);
				return;
			} else if(request.getParameter("lastname") == null || request.getParameter("lastname").isEmpty()) {
				request.setAttribute("errorChamplastname", "has-error");
				request.setAttribute("firstname", request.getParameter("firstname"));
				request.setAttribute("email", request.getParameter("email"));
				
				request.getRequestDispatcher("/register.jsp").forward(request, response);
				return;
			} else if(request.getParameter("firstname") == null || request.getParameter("firstname").isEmpty()) {
				request.setAttribute("errorChampfirstname", "has-error");
				request.setAttribute("lastname", request.getParameter("lastname"));
				request.setAttribute("email", request.getParameter("email"));
				request.getRequestDispatcher("/register.jsp").forward(request, response);
				return;
			}else if(request.getParameter("email") == null || request.getParameter("email").isEmpty()) {
				request.setAttribute("errorChampemail", "has-error");
				request.setAttribute("firstname", request.getParameter("firstname"));
				request.setAttribute("lastname", request.getParameter("lastname"));
				request.getRequestDispatcher("/register.jsp").forward(request, response);
				return;
			}else if(request.getParameter("password") == null || request.getParameter("password").isEmpty()) {
				request.setAttribute("errorChamppassword", "has-error");
				request.setAttribute("firstname", request.getParameter("firstname"));
				request.setAttribute("lastname", request.getParameter("lastname"));
				request.setAttribute("email", request.getParameter("email"));
				request.getRequestDispatcher("/register.jsp").forward(request, response);
				return;
			} else {
				jdbc.addMember(request.getParameter("lastname"), request.getParameter("firstname"), request.getParameter("email"), request.getParameter("password"));
				response.sendRedirect("/login.jsp");
				return;
			}
		} catch (SQLException e) {
			System.out.print(e.getMessage());
		}
	}

	public void destroy() {
		// do nothing.
	}
}
