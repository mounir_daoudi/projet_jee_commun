package fr.eservice.servlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.http.HttpServlet;

public class ConfigurationControl extends HttpServlet{

	private static final long serialVersionUID = 1L;

	public ConfigurationControl(){

	}

	public String getPath(){
		Properties properties=new Properties();

		try {
			FileInputStream in =new FileInputStream("config.properties");
			properties.load(in);
			in.close();
		} catch (IOException e) {
			System.out.println("Unable to load config file.");
		}
		return properties.getProperty("path","http://localhost:8080/projet_jee_commun");
	}

}
