package fr.eservice.servlet;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailControl {
	public String from;
	public String to;
	public String object;
	public String message;
	
	public String getFrom(){
		return this.from;
	}
	public String getTo(){
		return this.to;
	}
	public String getObject(){
		return this.object;
	}
	public String getMessage(){
		return this.message;
	}
	public void setFrom(String from){
		this.from = from;
	}
	public void setTo(String to){
		this.to = to;
	}
	public void setObject(String object){
		this.object= object;
	}
	public void setMessage(String message){
		this.message = message;
	}
	
	public static Message configMail(){
		final String username = "jeemail2015@gmail.com";
		final String password = "informatique";
 
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
 
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
		Message message = new MimeMessage(session);

		return message;
	}
	
	
	public static void sendMail(String to , String from, String messageTxt, String subject) {
		Message message = configMail();
		try {
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject(subject);
			message.setText(messageTxt);
 
			Transport.send(message);
 
			System.out.println("Done");
 
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}

