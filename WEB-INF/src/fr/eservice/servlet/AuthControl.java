package fr.eservice.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import jdbc.TestJDBC;

@WebServlet("/auth")
public class AuthControl extends HttpServlet {
	public static final String CHAMP_EMAIL = "email";
	public static final String CHAMP_PASS = "password";

	private static final long serialVersionUID = 1L;
	private TestJDBC jdbc;

	public void init() throws ServletException {
		jdbc = new TestJDBC();

		try {
			jdbc.connect();
		} catch (ClassNotFoundException e) {
			System.out.print(e.getMessage());
		} catch (SQLException e) {
			System.out.print(e.getMessage());
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Set response content type
		response.setContentType("text/html");
		
		/* R�cup�ration des champs du formulaire. */
		String email = request.getParameter( CHAMP_EMAIL );
		String password = request.getParameter( CHAMP_PASS );
		
		try {
			if (jdbc.checkPasswordWithSum(email,password)) {
				response.sendRedirect("/home.jsp");
				return;
			} else {
				request.setAttribute("error", "Incorrect email or password");
				request.setAttribute("email", request.getParameter("email"));
				request.getRequestDispatcher("/login.jsp").forward(request, response);
				return;
			}
		} catch (SQLException e) {
			System.out.print(e.getMessage());
		}
	}

	public void destroy() {
		// do nothing.
	}

}
