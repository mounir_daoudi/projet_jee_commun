package impl;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import api.dao.MemberDao;
import api.model.Member;


@Controller
@ComponentScan({"jpa","impl"})
public class AuthControl {

	@Autowired
	MemberDao dao;
	
	@RequestMapping( value="/login.html", method = RequestMethod.GET)
	public String login()
    {
        return "login";
    }
	
	@RequestMapping(value="/login.html", method = RequestMethod.POST)
	public String processLogin(@ModelAttribute("userForm") Member member,
           Model model,HttpSession session) {
		boolean in = dao.checkMailPwd(member.getEmail(), member.getPassword());
		if(in){
			member = dao.findByEmail(member.getEmail());
			session.setAttribute("memberCurrent", member);
            return "redirect:/auth.html";
		}
		model.addAttribute("error", "Incorrect email or password");
		model.addAttribute("email", member.getEmail());
		return "login";       
   }
}
