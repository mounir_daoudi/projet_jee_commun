package impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import api.dao.BoardGameDao;
import api.model.BoardGame;


@Controller
@ComponentScan({"jpa","impl"})
public class addGameControl {

	@Autowired
	BoardGameDao dao;
	
	@RequestMapping( value="/addGame.html", method = RequestMethod.GET)
	public String addGame() {
        return "addGame";
    }
	
	@RequestMapping(value="/addGame.html", method = RequestMethod.POST)
	public String processAddGame(@RequestParam("gameId")int gameId, Model model) {
		BoardGame game = new BoardGame();
		try {
			Document html = Jsoup.connect("http://boardgamegeek.com/boardgame/"+gameId).get();
			String[] title = html.title().split("[\\x7C]");

			if(!(title[0].equals("BoardGameGeek "))) {
				Element table = html.select("TABLE[class=geekitem_infotable]").first();
		        Elements rows = table.select("tr");
		        Element div = html.select("DIV[id=editdesc]").first();
		        
		        game.name = title[0];
		        game.description = div.text();
		        
		        for(int i = 0; i < rows.size(); i++) {
		        	switch (rows.get(i).children().get(0).text()) {
					case "Designer":
						String authors =  rows.get(i).children().get(1).text();
						model.addAttribute("authors", authors);
						break;
					case "Year Published":
						game.publicationYear =  Integer.parseInt(rows.get(i).children().get(1).text());
						break;
					case "# of Players":
						String[] nberPlayers = rows.get(i).children().get(1).text().split("\u00a0");
						game.minPlayer = Integer.parseInt(nberPlayers[0]);
				        if(nberPlayers.length > 1) {
				        	game.maxPlayer = Integer.parseInt(nberPlayers[2]);
				        } else {
				        	game.maxPlayer = Integer.parseInt(nberPlayers[0]);
				        }
						break;
					case "Mfg Suggested Ages":
						String[] suggestedAges = rows.get(i).children().get(1).text().split(" ",2);
						game.suggestedAge = Integer.parseInt(suggestedAges[0]);
						model.addAttribute("suggestedAgeFurtherInfo", suggestedAges[1]);
						break;
					case "Playing Time":
						String[] playingTime = rows.get(i).children().get(1).text().split(" ",2);
						game.playingTime = Integer.parseInt(playingTime[0]);
						model.addAttribute("timeUnit", playingTime[1]);
						break;
					case "Expansion":
						List<String> extentions = new ArrayList<String>();
						Elements td_divs = rows.get(i).children().get(1).
								children().get(1).children();
						
						if(td_divs.size() <= 4) {
							for(int j = 0; j < td_divs.size(); j++) {
								extentions.add(td_divs.get(j).text());
							}
						} else {
							for(int j = 0; j < 4; j++) {
								extentions.add(td_divs.get(j).text());
							}
							Elements td_divs_divs = td_divs.get(4).children();
							for(int j = 0; j < td_divs_divs.size(); j++) {
								extentions.add(td_divs_divs.get(j).text());
							}
						}
					
						model.addAttribute("extentions", extentions);
						break;
					default:
						break;
					}
		        }
				model.addAttribute("game", game);
			} else {
				model.addAttribute("error", "No game for this ID");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "addGame";
   }
}
