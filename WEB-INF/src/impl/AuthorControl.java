package impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import api.dao.AuthorDao;
import api.model.Author;

@Controller
@ComponentScan({"jpa","impl"})
public class AuthorControl {

	@Autowired
	AuthorDao dao;
	
	@RequestMapping( value="/authors.html", method = RequestMethod.GET)
	public String list(Model model) {
		// Creation of 2 authors for tests
		/*dao.create("Mohamed", "OUANNANE");
		dao.create("Amine", "AIT BELARABI");
*/
		List<Author> authors = dao.listAll();
		model.addAttribute("authors", authors);
		return "authors";
	}
}