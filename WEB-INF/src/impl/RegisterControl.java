package impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import fr.eservice.servlet.ConfigurationControl;
import fr.eservice.servlet.MailControl;
import api.dao.MemberDao;
import api.model.Member;

@Controller
@ComponentScan({"jpa","impl"})
public class RegisterControl {
	@Autowired
	MemberDao dao;
	
	@RequestMapping( value="/register.html", method = RequestMethod.GET)
    public String viewRegistration() {
        return "register";
    }
     
	@RequestMapping(value="/register.html", method = RequestMethod.POST)
    public String processRegistration(@ModelAttribute("userForm") Member member,
            Model model) {
		
		ConfigurationControl config = new ConfigurationControl();
		//Génération d'un id unique pour la confirmation de l'adresse mail
		String uid = UUID.randomUUID().toString();
		String message = config.getPath()+"/confirm.html?uid="+uid;
        if(dao.findByEmail(member.getEmail()) != null){
          	model.addAttribute("error","This mail is already registred");
          	model.addAttribute("errorChampemail", "has-error");
          	model.addAttribute("email",member.getEmail());
          	model.addAttribute("lastname",member.getLastname());
          	model.addAttribute("firstname",member.getFirstname());
          	return "register";
        }else{
          	model.addAttribute("success","Success Registration,check your email, you'll need to confirm your account");
        	dao.create(member.getEmail(), member.getFirstname(), member.getLastname(), member.getPassword(), uid);
			MailControl.sendMail(member.getEmail(), "no-reply@authentification.com", message, "confirmation de création de compte");
        }
        return "login";
    }
}