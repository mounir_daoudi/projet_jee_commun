package impl;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import api.dao.AuthorDao;
import api.dao.BoardGameDao;
import api.dao.MemberDao;
import api.model.Member;

@Controller
@ComponentScan({"jpa","impl"})
public class MemberControl {

	@Autowired
	MemberDao dao;
	@Autowired
	AuthorDao daoAuthor;
	@Autowired
	BoardGameDao boardGameDao;
	
	@RequestMapping(value="/auth.html", method = RequestMethod.POST)
	public String loadProfil(@RequestParam("idMember")String memberId, @RequestParam("action") String action, @RequestParam("idFriend") String friendId,
           Model model,HttpSession session) {
		
		if (friendId != "")
		{
			switch(action)
			{
			case "add":
				dao.addFriend(((Member) session.getAttribute("memberCurrent")).getId(),Integer.parseInt(friendId));
				break;
			case "del":
				dao.removeFriend(((Member) session.getAttribute("memberCurrent")).getId(),Integer.parseInt(friendId));
				break;
			case "logout":
				 session.setAttribute("memberCurrent",null);
				break;
			}
		}
		if ((Member) session.getAttribute("memberCurrent") != null){
		   	List<Member> friends = dao.listAllFriends(((Member) session.getAttribute("memberCurrent")).getId());
		   	List<Member> members = dao.listWithOutFriend(friends,(Member) session.getAttribute("memberCurrent"));
		   	model.addAttribute("friends", friends);
		   	model.addAttribute("members", members);
		   	return "home";  
		}
			return "login"; 
   }
	
	@RequestMapping( value="/auth.html", method = RequestMethod.GET)
	public String list(Model model,HttpSession session) {
		if ((Member) session.getAttribute("memberCurrent") != null){
		   	List<Member> friends = dao.listAllFriends(((Member) session.getAttribute("memberCurrent")).getId());
		   	List<Member> members = dao.listWithOutFriend(friends,(Member) session.getAttribute("memberCurrent"));
		   	model.addAttribute("friends", friends);
			model.addAttribute("members", members);
			return "home";
		}
		return "login";
	}
}