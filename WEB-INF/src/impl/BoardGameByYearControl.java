package impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import api.dao.BoardGameDao;
import api.model.BoardGame;

@Controller
@ComponentScan({"jpa","impl"})
public class BoardGameByYearControl {

	@Autowired
	BoardGameDao dao;
	
	@RequestMapping( value="/GamesByYear.html", method = RequestMethod.GET)
	   public String listAllGames(Model model)
	 {
		List<BoardGame> games = dao.listAll();
		model.addAttribute("games", games);
	     return "GamesByYear";
	 }
	
	@RequestMapping( value="/GamesByYear.html", method = RequestMethod.POST)
	public String research(@ModelAttribute("gamesYear") BoardGame boardGame,Model model) {
		List<BoardGame> games = dao.listAllGamesByYear(boardGame.publicationYear);
		model.addAttribute("games", games);
		return "GamesByYear";
	}
}