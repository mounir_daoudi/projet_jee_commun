package impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import api.dao.MemberDao;
import api.dao.PasswordChangeRequestsDao;
import api.model.Member;
import fr.eservice.servlet.ConfigurationControl;
import fr.eservice.servlet.MailControl;

@Controller
@ComponentScan({"jpa","impl"})
public class ForgotPasswordControl {

	@Autowired
	MemberDao dao;
	@Autowired
	PasswordChangeRequestsDao daoPwd;

	@RequestMapping( value="/forgotPassword.html", method = RequestMethod.GET)
	public String forgotPassword()
	{
		return "forgotPassword";
	}

	@RequestMapping(value="/forgotPassword.html", method = RequestMethod.POST)
	public String processLogin(@ModelAttribute("userForm") Member member,
			Model model) {
		ConfigurationControl config = new ConfigurationControl();
		String uidPwd = UUID.randomUUID().toString();
		Member mem = dao.findByEmail(member.getEmail());
		String message = config.getPath()+"/changePassword.html?uid="+uidPwd;

		
		if(mem != null){
			daoPwd.create(new java.util.Date(), uidPwd, 0, mem);
			MailControl.sendMail(member.getEmail(), "no-reply@authentification.com", message, "Modification de votre mot de passe");
			model.addAttribute("success", "Success Mail Sent; Please check your inbox");
		}else{
			model.addAttribute("error", "The email address you entered is not associated with any account");
		}
			return "forgotPassword";


	}
}
