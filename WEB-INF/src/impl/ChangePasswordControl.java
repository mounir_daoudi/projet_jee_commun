package impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import api.dao.MemberDao;
import api.dao.PasswordChangeRequestsDao;
import api.model.Member;
import api.model.PasswordChangeRequests;
import fr.eservice.servlet.MailControl;

@Controller
@ComponentScan({"jpa","impl"})
public class ChangePasswordControl {

	@Autowired
	MemberDao dao;
	@Autowired
	PasswordChangeRequestsDao daoPwd;

	@RequestMapping( value="/changePassword.html", method = RequestMethod.GET)
	public String changePassword(@RequestParam("uid") String uid, Model model)
	{
		PasswordChangeRequests founded = daoPwd.checkUid(uid);
		if(founded != null){
			daoPwd.changeAlreadyUsed(uid);
			model.addAttribute("id", founded.member.id);
			return "changePassword";
		}
		model.addAttribute("error", "Wrong Link for Changing password");
		return "login";
	}

	@RequestMapping(value="/changePassword.html", method = RequestMethod.POST)
	public String processLogin(@ModelAttribute("userForm") Member member,
			Model model) {
		
		int updated = dao.changePassword(member.getId(), member.getPassword());
		if(updated > 0){
			model.addAttribute("success", "Success Password updated ");
		}else{
			model.addAttribute("error", "Error during updated password. try Again later");
		}
		return "login";


	}
}
