package impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import api.dao.AuthorDao;
import api.dao.BoardGameDao;
import api.dao.MemberDao;

@Controller
@ComponentScan({"jpa","impl"})
public class AccountConfimationControl {

	@Autowired
	MemberDao dao;
	@Autowired
	AuthorDao daoAuthor;
	@Autowired
	BoardGameDao boardGameDao;
	@Autowired
	MemberDao memberDao;
	
	@RequestMapping( value="/confirm.html", method = RequestMethod.GET)
	public String confirm(@RequestParam("uid") String uid, Model model)
	{
		int updated = dao.confirmAccount(uid);
		if(updated == 0){
			model.addAttribute("error", "Confirmation code is invalid");
		}else{
			model.addAttribute("success", "confirmation Success, please log in.");
			
			boardGameDao.deleteAll();
			daoAuthor.deleteAll();
			
			long id1 = daoAuthor.create("Mohamed", "OUANNANE").id;
			long id2 = daoAuthor.create("Amine", "AIT BELARABI").id;
			
			// Creation of 3 games for test
			long idg1 = boardGameDao.create("Super mario", 1978, 0, 0, 0, 0, "For kids").id;
			long idg2 = boardGameDao.create("Need 4 speed", 2014, 0, 0, 0, 0, "Cars").id;
			long idg3 = boardGameDao.create("Fifa 15", 2014, 0, 0, 0, 0, "Football").id;
						
			boardGameDao.addAuthor(idg1,id1);
			boardGameDao.addAuthor(idg2,id1);
			boardGameDao.addAuthor(idg3,id2);
		}
		return "login";
	}

}
