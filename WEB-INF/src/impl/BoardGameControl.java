package impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import api.dao.AuthorDao;
import api.dao.BoardGameDao;
import api.model.Author;
import api.model.BoardGame;

@Controller
@ComponentScan({"jpa","impl"})
public class BoardGameControl {

	@Autowired
	BoardGameDao dao;
	@Autowired
	AuthorDao authordao;
	
	@RequestMapping( value="/authorGames.html", method = RequestMethod.GET)
	public String listAllGames(Model model) {
		
		List<BoardGame> games = dao.listAll();
		model.addAttribute("games", games);
		return "authorGames";
	}
	
	@RequestMapping( value="/authorGames.html", method = RequestMethod.POST)
	public String listGames(@RequestParam("idAuthor")long idAuthor, Model model) {
		Author author = authordao.find(idAuthor);
		List<BoardGame> Resultgames = new ArrayList<BoardGame>();
		
		List<BoardGame> games = dao.listAll();

		for (BoardGame temp : games) {
			if(temp.authors.contains(author)){
				Resultgames.add(temp);
			}
		}
		model.addAttribute("games", Resultgames);
		model.addAttribute("author", author);
		return "authorGames";
	}
}