<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="api.model.BoardGame" %>

<html lang="en">
	 <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="Games" content="">
		<link rel="icon" href="http://getbootstrap.com/favicon.ico">
		<title>Author Games list</title>
		<!-- Bootstrap core CSS -->
		<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="signin.css" rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="http://getbootstrap.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="http://getbootstrap.com/assets/js/ie-emulation-modes-warning.js"></script>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		   <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
		<script>
			$( document ).ready(function() {
				$( "#searchGame" ).keyup(function() {
					var text = 	$("#searchGame").val();
				  	$("#allGames tr").hide();
				  	$("#allGames tr:first").show();
					$("#allGames tr:contains('"+text+"')").show();
				});
				
			});
		</script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
	 </head>
	<body>
		<div class="container">
			<div class="panel panel-default">        
				<!-- Default panel contents -->
				<p>Author : ${author.lastname} ${author.firstname}</p>
				<div class="panel-heading">Games list</div>
				<c:choose>
					<c:when test="${empty games}">
					   Games list empty
					</c:when>
					<c:otherwise>
					   <!-- Table -->
		            	<table class="table" id="allGames">
			            	<tr>
								<th>Id </th>
								<th>Name </th>
								<th>Description </th>
								<th>Publication year </th>
							</tr>	
				            <tr>
				            	<input class="form-control" type="text" style="width:100%;" colspan="4" id="searchGame" placeHolder="search ... ">
				            </tr>
							<c:forEach items="${games}" var="game">
								<tr>
								   <td>${game.id}</td>
								   <td>${game.name}</td>
								   <td>${game.description}</td> 
								   <td>${game.publicationYear}</td>
								</tr>
							</c:forEach>
						</table>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</body>
</html>
