<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="api.model.Author" %>

<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="http://getbootstrap.com/favicon.ico">
		<title>Authors list</title>
		<!-- Bootstrap core CSS -->
		<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="signin.css" rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="http://getbootstrap.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="http://getbootstrap.com/assets/js/ie-emulation-modes-warning.js"></script>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]--> 
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
		<script>
			function post(idAuthor)
			{
				document.getElementById('idAuthor').value= idAuthor;      		
				document.forms["formulaire"].submit();
			}
		</script>
	</head>
	<body>
	   <div class="container">
	   	<form:form name="formulaire" action="authorGames.html" method="POST" commandName="formulaire1">
	   		<input type="hidden" id="idAuthor" name ="idAuthor"/>
	   	</form:form>
	      <div class="panel panel-default">        
	         <!-- Default panel contents -->
	         <div class="panel-heading">Authors list</div>
	         <c:choose>
	            <c:when test="${empty authors}">
	               Authors list empty
	            </c:when>
	            <c:otherwise>
	               <!-- Table -->
	               <table class="table">
	                  <c:forEach items="${authors}" var="author">
	                     <tr>
	                        <td>${author.id}</td> 	
	                        <td>${author.firstname}</td> 
	                        <td>${author.lastname}</td>
	                        <td align="right"><button class="btn btn-sm btn-primary btn-block" onclick="post('${author.id}');">Games list</button></td>
	                     </tr>
	                  </c:forEach>
	               </ul>
	            </c:otherwise>
	         </c:choose>
	      </div>
	   </div>
	</body>
</html>
