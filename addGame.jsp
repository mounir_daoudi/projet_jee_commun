<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html lang="en">
   	<head>
    	<meta charset="utf-8">
      	<meta http-equiv="X-UA-Compatible" content="IE=edge">
      	<meta name="viewport" content="width=device-width, initial-scale=1">
      	<meta name="description" content="">
      	<meta name="Games" content="">
      	<link rel="icon" href="http://getbootstrap.com/favicon.ico">
      	<title>Add Game form</title>
      	<!-- Bootstrap core CSS -->
      	<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
      	<!-- Custom styles for this template -->
      	<link href="signin.css" rel="stylesheet">
      	<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
      	<!--[if lt IE 9]><script src="http://getbootstrap.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
      	<script src="http://getbootstrap.com/assets/js/ie-emulation-modes-warning.js"></script>
      	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      	<!--[if lt IE 9]>
         	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
         	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      	<![endif]-->
      	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      	<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
   	</head>
   	<body>
		<div class="container">
			<c:if test="${!empty error}">
				<center><p><span class="alert alert-danger">${error}</span></p></center>
			</c:if>
      		<form:form action="addGame.html" method="post" class="form-addGame" >
				<h2 class="form-addGame-heading">ADD GAMES BY ID FROM BOARDGAMEGEEK.COM</h2>
				<label for="inputId" class="sr-only">id</label>
				<input type="text" id="gameId" name ="gameId" class="form-control" placeholder="Game Id from http://boardgamegeek.com" required="" autofocus="">
				<button class="btn btn-lg btn-primary btn-block" type="submit">Search</button>
			</form:form>
         	<div class="panel panel-default">        
            	<!-- Default panel contents -->
            	<div class="panel-heading">Game information</div>
	            <c:choose>
	            	<c:when test="${empty game}">
	                	<form:form class="form-addGame" >
	                		<label for="inputName" class="">Name</label>
							<input type="text" id="inputName" name ="inputName" class="form-control" placeholder="Name" required="" autofocus="">
							<label for="inputAuthors" class="">Authors</label>
							<input type="text" id="inputAuthors" name ="inputAuthors" class="form-control" placeholder="Authors" required="" autofocus="">
							<label for="inputPublicationYear" class="">Publication Year</label>
							<input type="text" id="inputPublicationYear" name ="inputPublicationYear" class="form-control" placeholder="Publication Year" required="" autofocus="">
							<label for="inputDescription" class="">Description</label>
							<textarea id="inputDescription" name="inputDescription" class="form-control" placeholder="Description" rows="7" required="" autofocus=""></textarea>
							<label for="inputExtentions" class="">Extentions</label>
							<input type="text" id="inputExtentions" name ="inputExtentions" class="form-control" placeholder="Extentions" required="" autofocus="">
							<label for="inputMaxPlayer" class="">Max Player</label>
							<input type="text" id="inputMaxPlayer" name ="inputMaxPlayer" class="form-control" placeholder="Max Player" required="" autofocus="">
							<label for="inputMinPlayer" class="">Min Player</label>
							<input type="text" id="inputMinPlayer" name ="inputMinPlayer" class="form-control" placeholder="Min Player" required="" autofocus="">
							<label for="inputPlayingTime" class="">Playing Time</label>
							<input type="text" id="inputPlayingTime" name ="inputPlayingTime" class="form-control" placeholder="Playing Time" required="" autofocus="">
							<label for="inputSuggestedAge" class="">Suggested Age</label>
							<input type="text" id="inputSuggestedAge" name ="inputSuggestedAge" class="form-control" placeholder="Suggested Age" required="" autofocus="">
						</form:form>
	               	</c:when>
	               	<c:otherwise>
	                	<form:form class="form-addGame" >
	                		<label for="inputName" class="">Name</label>
							<input type="text" id="inputName" name ="inputName" class="form-control	" placeholder="Name" required="" autofocus="" value="${game.name}">
							<label for="inputAuthors" class="">Authors</label>
							<input type="text" id="inputAuthors" name ="inputAuthors" class="form-control" placeholder="Authors" required="" autofocus="" value="${authors}">
							<label for="inputPublicationYear" class="">Publication Year</label>
							<input type="text" id="inputPublicationYear" name ="inputPublicationYear" class="form-control" placeholder="Publication Year" required="" autofocus="" value="${game.publicationYear}">
							<label for="inputDescription" class="">Description</label>
							<textarea id="inputDescription" name="inputDescription" class="form-control" placeholder="Description" rows="7" required="" autofocus="">${game.description}</textarea>
							<label for="inputExtentions" class="">Extentions</label>
							<textarea id="inputExtentions" name ="inputExtentions" class="form-control" placeholder="Extentions" rows="7" required="" autofocus=""><c:if test="${!empty extentions}"><c:forEach var="i" begin="0" end="${extentions.size()-1}"><c:out value="${extentions.get(i)}" /><% out.print("\n"); %></c:forEach></c:if></textarea>
							<label for="inputMaxPlayer" class="">Max Player</label>
							<input type="text" id="inputMaxPlayer" name ="inputMaxPlayer" class="form-control" placeholder="Max Player" required="" autofocus="" value="${game.maxPlayer}">
							<label for="inputMinPlayer" class="">Min Player</label>
							<input type="text" id="inputMinPlayer" name ="inputMinPlayer" class="form-control" placeholder="Max Player" required="" autofocus="" value="${game.minPlayer}">
							<label for="inputPlayingTime" class="">Playing Time</label>
							<input type="text" id="inputPlayingTime" name ="inputPlayingTime" class="form-control" placeholder="Playing Time" required="" autofocus="" value="${game.playingTime} ${timeUnit}">
							<label for="inputSuggestedAge" class="">Suggested Age</label>
							<input type="text" id="inputSuggestedAge" name ="inputSuggestedAge" class="form-control" placeholder="Suggested Age" required="" autofocus="" value="${game.suggestedAge} ${suggestedAgeFurtherInfo}">
						</form:form>
	               	</c:otherwise>
	         	</c:choose>
         	</div>
      	</div>
   	</body>
</html>
