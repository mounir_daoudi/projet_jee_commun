# README #

### Etudiants ###

Groupe 2 :
Mounir DAOUDI
Mohamed OUANNANE
Mohamed-Amine AIT BELARABI
Ayoub NEJMEDDINE

### Les fonctionnalités développées  ###

- Un internaute doit pouvoir s'inscrire à notre méta-portail. Le nom, l'email et le mot de passe est obligatoire,
- Un Membre doit pouvoir s'identifier sur la plateforme (mail / mot de passe),
- Si un Membre a oublié son mot de passe il peut demander à recevoir un mail lui permettant de changer son mot de passe,
- Un membre peut voir sa fiche sur laquelle on voit son nom, prénom. Un lien vers sa liste de jeux (voir partie ludothèque),
- Un membre peut ajouter/supprimer un autre membre en "amis". Il apparaît dans sa liste d'amis et il peut facilement accéder à sa fiche,
- L'admin peut indiquer l'identifiant d'un jeu sur "boardgamegeek.com" afin d'afficher un formulaire d'ajout de jeu pré-rempli automatiquement avec les infos provenant de la page du jeu sur gameboardgeek. exemple : Carcassonne ID 822, page : http://boardgamegeek.com/boardgame/822/carcassonne,
- Un membre peut afficher la liste des jeux d'un auteur,
- Un membre peut lister les jeux en fonction de son année de sortie.

### Déploiement et tests avec jeu de données ###
## Fichier de configuration ##
Vous trouverez un fichier nommé "config.properties" dans la racine de projet, déplacez le dans tomcat8/bin et modifiez le nom de path de votre projet pour pouvoir envoyer les mails avec le bon chemin de projet.

## Remarques ##

Toutes les fonctionnalités ont été développées avec leurs tests unitaires
