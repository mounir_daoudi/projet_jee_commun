<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="api.model.Member" %>

<!-- saved from url=(0064)http://gdufrene.github.io/java_ee_spring-14/files/tp2/login.html -->
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="http://getbootstrap.com/favicon.ico">
		
		<title>Home</title>
		
		<!-- Bootstrap core CSS -->
		<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="http://gdufrene.github.io/java_ee_spring-14/files/tp2/signin.css" rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="http://getbootstrap.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="./Signin Template for Bootstrap_files/ie-emulation-modes-warning.js"></script>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		   <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      	   <script src="./Signin Template for Bootstrap_files/ie10-viewport-bug-workaround.js"></script>
		   <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
		<script>
			$( document ).ready(function() {
				
				$( "#searchFriend" ).keyup(function() {
					var text = 	$("#searchFriend").val();
				  	$("#friendTable tr").hide();
					$("#friendTable tr:contains('"+text+"')").show();
				});
				$( "#searchMember" ).keyup(function() {
					var text = 	$("#searchMember").val();
				  	$("#memberTable tr").hide();
					$("#memberTable tr:contains('"+text+"')").show();
				});
			});
				function post(action,idMember,idFriend)
				{
					document.getElementById('idFriend').value= idFriend;
					document.getElementById('idMember').value= idMember;
					document.getElementById('action').value= action;         		
					document.forms["profilForm"].submit();
				}
				function chargerFiche(id,prenom,nom,email){
					document.getElementById('id').innerHTML= id;
					document.getElementById('prenom').innerHTML= prenom;
					document.getElementById('nom').innerHTML= nom; 
					document.getElementById('email').innerHTML= email;         		
			       $("#myModal").modal('show');
			   }
		</script>
	</head>
   <body>
		<div class="container">
			<h2>CURRENT PROFIL</h2>
			<p style="float:left;">User Name : ${memberCurrent.firstname}  ${memberCurrent.lastname}</p>
			<p style="float:left;margin-left:50px;">Email : ${memberCurrent.email}</p>
			<p style="float:left;margin-left:50px;">Games list : <a href="#">Link to list</a> </p>
			<p style="float:left;margin-left:50px;">Authors list : <a href="authors.html">Link to list</a> </p>  
			<p style="float:left;margin-left:50px;">(<a onclick="post('logout','-1','-1');" href="#">Logout</a>) </p>    
			<br>
			<form:form name="profilForm" action="auth.html" method="post" commandName="profilForm">
				<input type="hidden" id="idFriend" name ="idFriend"/>
				<input type="hidden" id="action" name ="action"/>
				<input type="hidden" id="idMember" name ="idMember"/>
			</form:form>
			<div class="panel panel-default" style="height:75%;width:40%;float:left;overflow-y:auto;">        
				<!-- Default panel contents -->
				<div class="panel-heading">Friends list :</div>
				<c:choose>
				   <c:when test="${empty friends}">
				      Friend list empty
				   </c:when>
				   <c:otherwise>
				      <!-- Table -->
				      <table class="table" id="friendTable">
				      	<tr><input class="form-control" type="text" style="width:100%;" colspan="4" id="searchFriend" placeHolder="search ... "></tr>
				         <c:forEach items="${friends}" var="friend">
				            <tr style="cursor:pointer" onclick="chargerFiche('${friend.id}','${friend.firstname}','${friend.lastname}','${friend.email}')">
				               <td>${friend.id}</td>
				               <td>${friend.firstname}</td> 
				               <td>${friend.lastname}</td>
				          	   <td align="right"><button class="btn btn-sm btn-primary btn-block" onclick="post('del','${memberCurrent.id}','${friend.id}');">Retirer Ami</button></td>
				            </tr>
				         </c:forEach>
				      </table>
				   </c:otherwise>
				</c:choose>
		   </div>
		   <div class="panel panel-default" style="height:75%;width:40%;margin-left:50px;float:left;overflow-y:auto;">
				<div class="panel-heading">Members list :</div>
				   <c:choose>
				   <c:when test="${empty members}">
				      Members list empty
				   </c:when>
				   <c:otherwise>
				      <!-- Table -->
				      	<tr><input type="text" class="form-control" style="width: 100%;" colspan="4" id="searchMember" placeHolder="search ... "></tr>
				      <table class="table" id="memberTable">
				         <c:forEach items="${members}" var="member">
				            <tr>
				               <td>${member.id}</td>
				               <td>${member.firstname}</td> 
				               <td>${member.lastname}</td>
				               <td align="right"><button class="btn btn-sm btn-primary btn-block" onclick="post('add','${memberCurrent.id}','${member.id}');">Ajouter Ami</button></td>
				            </tr>
				         </c:forEach>
				      </table>
				   </c:otherwise>
				</c:choose>
				</div>
			</div>
		</div> 
	
		<div id="myModal" class="modal fade">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title">Member profil :</h4>
		            </div>
		            <div class="modal-body">
		                <p>Id :<label id="id">25</label></p>
		                <p>Firstname      :<label id="prenom">25</label></p>
		                <p>Lastname         :<label id="nom">25</label></p>
		                <p>Email       :<label id="email">25</label></p>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		            </div>
		        </div>
		    </div>
		</div>
   </body>
</html>